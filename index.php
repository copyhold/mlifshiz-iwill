<?php
if ($_POST) {
  $message = strtr("
    שם: fname lname
    Email: email
    טלפון: tel
    מיספר אורחים: parti
    ",$_POST);
  $to = 'tzvir@IsraelExperts.com , daphna@israelexperts.com , israelexperts.gibuy@gmail.com'; 
  mb_internal_encoding("UTF-8");
  mb_send_mail($to, 'landing page form', $message, "Mime-Version: 1.0\nContent-type: text/plain;charset=utf-8 \r\nReply-To: " . $_POST['email'] . "\r\n");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width">
<meta charset="UTF-8">
  <title></title>
<link href='http://fonts.googleapis.com/css?family=Crete+Round:400italic,400|Open+Sans:300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="style.css">
</head>
<body class="<?php echo ($_GET['thankyou'] ? 'thankyou' : 'regular') ; ?>">
<header>
  <a class="logo-iwill" href="iwill-site"><img src="logo-iw.png" alt=""></a>
  <a class="logo-ie" href="https://israelexperts.co.il"><img src="logo-ie.svg" alt=""></a>
  <div class="so">
    <a class=in target=_blank href="http://instagram.com/israelexperts">instagram</a>
    <a class=tw target=_blank href="https://twitter.com/TaglitIsraelExp">twitter</a>
    <a class=fb target=_blank href="https://www.facebook.com/TaglitIsraelExperts">facebook</a>
  </div>
</header>
<?php if ($_GET['thankyou']): ?>
<div class="thankyou">
  <h1>THANK YOU!</h1>
  <p>Thank yu for your interest, We received your request and will call you back soon.</p>
</div>
<script>setTimeout(function() { location.href = "http://www.israelexperts.com/" }, 4000); </script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1025640144;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "xB6FCNHt-1wQ0I2I6QM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1025640144/?label=xB6FCNHt-1wQ0I2I6QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php else: ?>
  <div>
    <h2>Israel With Lori Lasday –<br>
IWiLL in partnership with IsraelExperts </h2>
    <h3>IWILL 101:</h3>
<p>February 11-20, 2016<br> 
May 19-28, 2016<br>
June 30-July 9, 2016</p>
  <a href="#form">CLICK HERE FOR TRIP ITINERARY</a>
<h3>IWILL Specialty:</h3>
<p> November 2016 </p>
  </div>

<form action="?thankyou=1" method=post>
  <h2>Leave your info, we’ll do the rest</h2>
  <input required id=form type="text" name=fname placeholder="First Name">
  <input required type="text" name=lname placeholder="Last Name">
  <input type="number" name=parti placeholder="Number of Participants in Party">
  <input required type="email" name=email placeholder="Email Address">
  <input required type="tel" name=phone placeholder="Telephone Number">
  <button type="submit">JOIN US</button>
</form>
<main>
<h3>
  What is the IWILL Experience?
</h3>
<p>
  IWILL is a 10 day travel experience for adults age 50+. We have trips for both ACTIVE adults which include full days of on and off bus travel and plenty of walking, and LOW IMPACT trips for those who are less physically active and desire a more even-paced experience. Our meaningful Jewish journeys are for individuals, couples, relatives and friends from all “shuls of thought” – from secular to traditional.  In sharing this life changing experience, participants will be encouraged to build a sense of community that will last beyond the duration of this trip and beyond the borders of Israel.
</p>
<h3>
  Who is it for?
</h3>
<p>IWiLL 101 is designed for first time visitors to Israel and those individuals who have not been to Israel in more than 10 years.  Our programs take participants north, south, east and west, to cultivate a positive and long lasting relationship with the History, Future, Land and People of Israel. </p>
<p>IWiLL Specialty serves the desires and interests of seasoned active adult travelers to Israel. Each year the specialty tour will focus on a distinct subject to include: culinary and culture; in the footsteps of Biblical personalities; the borders of Israel and her neighbors, etc.  All accommodations will be five-star.</p>
<h3>
  How is this trip different from all other trips?
</h3>
<p>Each trip is staffed by a TEAM of Tour Guide, Bus Driver, and Lori Lasday, your inspirational, educational and lighthearted concierge. This bi-lingual team will work together to share their love, passion, knowledge, questions, and awe while insuring your enjoyment, sense of wonder, personal connection, health and safety.</p>
<p>All IWiLL participants will be given the opportunity to extend their stay in Israel with an optional 2 day trip to Eilat and Petra, Jordan, or an opportunity to give back to Israel and her citizens with 5 days of volunteer service. </p>
<p>Please let us know if you have a “dream trip” you would like to see offered. The Zionist leader Theodore Herzl said it best, “If you will it, it is not a dream.” With a group of 20-40 participants, we will work with you and your group to build the trip you have always imagined and bring it to life.</p>
<h3>
  What are the dates?
</h3>
<h2> IWILL 101: </h2>
<p>February 11-20, 2016 </p>
<p>May 19-28, 2016</p>
<p>June 30-July 9, 2016</p>
<br>
<h2> IWILL Specialty: </h2>
<p> November 2016 </p>
<hr>
<h3> I WILL </h3>
<ul>
  <li>Provide you with excellent and personalized service</li>
  <li>Connect you to the people and the Land of Israel</li>
  <li>Expand your horizons</li>
  <li>Nurture your body and soul</li>
  <li>Awaken all of your senses</li>
  <li>Encourage you to try new things</li>
  <li>Share the secrets of the historical  and mystical</li>
  <li>I WILL so that YOU Will</li>
</ul>
</main>
<?php endif; ?>                         
</body>                                 
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-13204786-7', 'auto');
ga('send', 'pageview');

</script>
</html>
